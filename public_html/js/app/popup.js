var popup = (function () {

    function init() {
        $('.pnt-js-new-tab').click(newTab);
    }

    function newTab() {
        chrome.tabs.create({});
        return false;
    }

    return {
        init: init
    };

})();

$(document).ready(popup.init);